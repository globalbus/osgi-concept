package info.globalbus.osgi.contract;

import lombok.Value;

@Value
public class LogEvent {
    private Object source;
    private String message;
}
