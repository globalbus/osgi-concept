package info.globalbus.osgi.contract;

public interface LogListener {
	void performLogEvent(LogEvent evt);
}
