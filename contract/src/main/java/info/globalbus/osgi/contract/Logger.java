package info.globalbus.osgi.contract;

public interface Logger {

	void log(Object src, String msg);

	void addLogListener(LogListener listener);

	void removeLogListener(LogListener listener);
}
