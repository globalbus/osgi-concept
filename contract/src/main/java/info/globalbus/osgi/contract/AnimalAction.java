package info.globalbus.osgi.contract;

/**
 * Pewną czynność do wykonania na zwierzęciu, np. karmienie.
 */
public interface AnimalAction {

	/**
	 * Wykonuje daną czynność na konkretnym zwierzęciu.
	 *
	 * @param animal zwierzę poddane danej czynności
	 * @return informacja o tym, czy czynność się udała (np. zwierzę zostałao nakarmione i nie zdechło)
	 */
	boolean execute(Animal animal);
}
