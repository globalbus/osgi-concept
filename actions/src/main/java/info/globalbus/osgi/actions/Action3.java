/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.globalbus.osgi.actions;

import info.globalbus.osgi.contract.Animal;
import info.globalbus.osgi.contract.AnimalAction;

/**
 * @author globalbus
 */
public class Action3 implements AnimalAction {
    public boolean execute(Animal animal) {
        animal.setStatus("akcja 3");
        return true;
    }

    public String toString() {
        return "akcja 3";
    }
};
