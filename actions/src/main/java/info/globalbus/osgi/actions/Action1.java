/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.globalbus.osgi.actions;

import info.globalbus.osgi.contract.Animal;
import info.globalbus.osgi.contract.AnimalAction;

/**
 * @author globalbus
 */
public class Action1 implements AnimalAction {
    public boolean execute(Animal animal) {
        animal.setStatus("akcja 1");
        return true;
    }

    public String toString() {
        return "akcja 1";
    }
};
