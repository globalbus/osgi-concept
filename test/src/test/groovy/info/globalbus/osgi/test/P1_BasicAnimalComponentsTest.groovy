package info.globalbus.osgi.test

import org.apache.felix.scr.ScrService
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.ops4j.pax.exam.Configuration
import org.ops4j.pax.exam.CoreOptions
import org.ops4j.pax.exam.Option
import org.ops4j.pax.exam.junit.PaxExam
import org.osgi.framework.BundleContext
import org.osgi.framework.Constants

import javax.inject.Inject

@RunWith(PaxExam)
public class P1_BasicAnimalComponentsTest {

	@Inject
	BundleContext context

	@Inject
	ScrService service

	def events

	def animalClassName = 'info.globalbus.osgi.contract.Animal'

	@Configuration
	Option[] configure() {
		[
                CoreOptions.mavenBundle('org.apache.felix', 'org.apache.felix.scr', '1.8.4'),
                CoreOptions.junitBundles(),
                CoreOptions.mavenBundle('org.codehaus.groovy', 'groovy-all', '2.4.13'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'contract'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'logger'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'common'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'animal1'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'animal2'),
                CoreOptions.mavenBundle('info.globalbus.osgi', 'animal3'),
		] as Option[]
	}

	@Before
	void setup() {
		def loggerService = context.getServiceReference('info.globalbus.osgi.contract.Logger')
		def logger = context.getService(loggerService)

		def contractBundle = context.bundles.find {
			it.symbolicName == 'info.globalbus.osgi.contract'
		}
		//def loggerClass = loggerBundle.loadClass('info.globalbus.osgi.logger.Logger')
		def logListenerClass = contractBundle.loadClass('info.globalbus.osgi.contract.LogListener')
		events = []
		def listener = { evt -> events << evt }.asType(logListenerClass)
		logger.addLogListener(listener)
	}

	@Test
	void "Animal1 component should exist in its own bundle"() {
		testAnimalExistence 'info.globalbus.osgi.animal1'
	}

	@Test
	void "Animal2 component should exist in its own bundle"() {
		testAnimalExistence 'info.globalbus.osgi.animal2'
	}

	@Test
	void "Animal3 component should exist in its own bundle"() {
		testAnimalExistence 'info.globalbus.osgi.animal3'
	}

	@Test
	void "Animal1 should inform about incoming and outgoing"() {
		testAnimalInOut 'info.globalbus.osgi.animal1'
	}

	@Test
	void "Animal2 should inform about incoming and outgoing"() {
		testAnimalInOut 'info.globalbus.osgi.animal2'
	}

	@Test
	void "Animal3 should inform about incoming and outgoing"() {
		testAnimalInOut 'info.globalbus.osgi.animal3'
	}

	private void testAnimalInOut(String symbolicName) {
		// given
		def bundle = context.bundles.find {
			it.symbolicName == symbolicName
		}
		def component = service.getComponents(bundle).find {
			animalClassName in it.services
		}
		def ref = bundle.registeredServices.find { def prop = it.getProperty(Constants.OBJECTCLASS); prop == animalClassName || animalClassName in prop }
		context.getService(ref)

		// when
		component.enable()
		component.disable()

		//then
		assert events.size() == 2
		Assert.assertEquals events[0].source, events[1].source
	}

	private void testAnimalExistence(String symbolicName) {
		// given
		def bundle = context.bundles.find {
			it.symbolicName == symbolicName
		}
		def component = service.getComponents(bundle).find {
			animalClassName in it.services
		}

		//then
		assert bundle
		assert component
	}
}
