package info.globalbus.osgi.common;

import info.globalbus.osgi.contract.Animal;
import info.globalbus.osgi.contract.Logger;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;

/**
 * @author globalbus
 */
public class AnimalInstance implements Animal {
    private String species;
    private String name;
    private String status;
    private final PropertyChangeSupport listeners;
    private final AtomicReference<Logger> logger = new AtomicReference<>();

    public AnimalInstance() {
        listeners = new PropertyChangeSupport(this);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSpecies() {
        return species;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        String oldValue = this.status;
        this.status = status;
        listeners.firePropertyChange(new PropertyChangeEvent(this, "status", oldValue, status));
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);
    }

    void activate(BundleContext bc, ComponentContext ctx, Map dict) {
        this.status = (String) ctx.getProperties().get("status");
        this.name = (String) ctx.getProperties().get("name");
        this.species = (String) ctx.getProperties().get("species");
        if (logger != null) {
            logger.get().log(this, species + " przybywa");
        }
    }

    void bindLogger(Logger logger) {
        this.logger.set(logger);
    }

    void deactivate() {
        if (logger != null) {
            logger.get().log(this, species + " odchodzi");
        }
    }

    void unbindLogger(Logger logger) {
        this.logger.set(null);
    }
}
