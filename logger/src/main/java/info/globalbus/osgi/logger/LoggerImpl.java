package info.globalbus.osgi.logger;

import info.globalbus.osgi.contract.LogEvent;
import info.globalbus.osgi.contract.LogListener;
import info.globalbus.osgi.contract.Logger;
import java.util.ArrayList;
import java.util.List;

public class LoggerImpl implements Logger {

    private List<LogListener> listeners;

    public LoggerImpl() {
        listeners = new ArrayList<>();
    }

    public void addLogListener(LogListener listener) {
        listeners.add(listener);
    }

    public void log(Object src, String msg) {
        LogEvent evt = new LogEvent(src, msg);
        for (LogListener listener : listeners) {
            listener.performLogEvent(evt);
        }
    }

    public void removeLogListener(LogListener listener) {
        listeners.remove(listener);
    }
}
