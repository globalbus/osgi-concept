package info.globalbus.osgi.zoo.internal;

import info.globalbus.osgi.contract.Animal;
import info.globalbus.osgi.contract.AnimalAction;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

public class ActionStub {

    private ComponentContext context;
    private String refName;
    private ServiceReference ref;

    private String actionName;

    private AnimalAction instance;

    public ActionStub(ComponentContext context, String refName, ServiceReference ref) {
        this.context = context;
        this.refName = refName;
        this.ref = ref;

        actionName = (String) ref.getProperty("name");
    }

    public boolean execute(Animal animal) {
        return get().execute(animal);
    }

    public AnimalAction get() {
        if (instance == null) {
            instance = (AnimalAction) context.locateService(refName, ref);
        }
        return instance;
    }

    public String toString() {
        return actionName;
    }
}
