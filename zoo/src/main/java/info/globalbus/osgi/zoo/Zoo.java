package info.globalbus.osgi.zoo;

import info.globalbus.osgi.contract.Animal;
import info.globalbus.osgi.zoo.internal.ActionStub;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;

public class Zoo {

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private Set<Animal> animals = new HashSet<>();
    private Set<ServiceReference> refs = new HashSet<>();
    private ComponentContext componentContext;

    public void activate(ComponentContext componentContext) {
        this.componentContext = componentContext;
    }

    public void addAction(ServiceReference ref) {
        refs.add(ref);

    }

    public void addAnimal(Animal animal) {
        Set<Animal> oldAnimals = animals;
        animals = new HashSet<>();
        animals.addAll(oldAnimals);
        if (animals.add(animal)) {
            pcs.firePropertyChange("animals", oldAnimals, animals);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public Set<ActionStub> getActionsFor(Collection<Animal> animals) {
        boolean first = true;
        Set<ServiceReference> actions = new HashSet<>();
        for (Animal animal : animals) {
            if (first) {
                actions.addAll(getActionsFor(animal));
                first = false;
            } else {
                actions.retainAll(getActionsFor(animal));
            }
        }
        Set<ActionStub> stubs = new HashSet<>();
        for (ServiceReference ref : actions) {
            stubs.add(new ActionStub(componentContext, "action", ref));
        }
        return stubs;
    }

    public Set<ServiceReference> getActionsFor(Animal animal) {
        Set<ServiceReference> result = new HashSet<>();
        for (ServiceReference ref : refs) {
            if (ref.getProperty("species") == null) {
                result.add(ref);
            } else if (ref.getProperty("species").equals(animal.getSpecies())) {
                result.add(ref);
            }
        }
        return result;
    }

    public Set<Animal> getAnimals() {
        return Collections.unmodifiableSet(animals);
    }

    public int getAnimalsCount() {
        return animals.size();
    }

    public void removeAction(ServiceReference ref) {
        refs.remove(ref);
    }

    public void removeAnimal(Animal animal) {
        Set<Animal> oldAnimals = animals;
        animals = new HashSet<>();
        animals.addAll(oldAnimals);
        if (animals.remove(animal)) {
            pcs.firePropertyChange("animals", oldAnimals, animals);
        }
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

}
